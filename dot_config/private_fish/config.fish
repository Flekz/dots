abbr -a o xdg-open

abbr -a gs git status
abbr -a ga git add
abbr -a gcam git commit -am
abbr -a gcm git commit -m

set -x XDG_HOME ~
set -x XDG_CONFIG_HOME $XDG_HOME/.config
set -x EDITOR hx

fish_vi_key_bindings
set fish_cursor_default block
set fish_cursor_insert line
set fish_cursor_replace_one underscore
set fish_cursor_visual block

function _command_exists
    command -v $argv >/dev/null
end

if status is-interactive
    # Commands to run in interactive sessions can go here

    if _command_exists atuin
        atuin init fish | source
    end

    # Start zellij in CLI container
    # if test $HOSTNAME = "cli.fedora"
    #     eval (zellij setup --generate-auto-start fish | string collect)
    # end
end

function removepath
    if set -l index (contains -i $argv[1] $PATH)
        set -e PATH[$index]
    else
        echo "$argv[1] not found in PATH: $PATH"
    end
end

# Fix PATH if in a container
if test $container
    removepath /home/flkz/.local/bin
    removepath /home/flkz/bin
    fish_add_path ~/.local/bin
    fish_add_path ~/bin
end


if test -f /usr/local/cargo/env
    set -x CARGO_HOME /usr/local/cargo
    set -x RUSTUP_HOME /usr/local/rustup
    fish_add_path $CARGO_HOME/bin
end


if _command_exists sk
    abbr -a rgsk sk --ansi -i -c \"rg --color=always --line-number '{}'\"
end

# --- CD/LS ---
if _command_exists eza
    set -l EZA_OPTS --group --header --group-directories-first --icons auto
    # CD functions
    abbr -a l eza $EZA_OPTS
    abbr -a ls eza $EZA_OPTS -a
    abbr -a ll eza $EZA_OPTS -la
    # Tree functions
    abbr -a lt eza $EZA_OPTS --tree --level
    abbr -a lr eza $EZA_OPTS --recursive --level
    function list_dir --on-variable PWD
        eza $EZA_OPTS -a
    end
else
    abbr -a l ls -l
    abbr -a ll ls -la
    function list_dir --on-variable PWD
        ls -GF -a
    end
end

# At _end_
if _command_exists zoxide
    zoxide init fish | source
    abbr -a cd z
end
if _command_exists direnv
    direnv hook fish | source
end


function _gen_completions
    echo installing Atuin completions
    atuin gen-completions --shell fish --out-dir $XDG_CONFIG_HOME/fish/completions

    echo installing zellij completions
    zellij setup --generate-completion fish >$XDG_CONFIG_HOME/fish/completions/zellij.fish

    echo installing rustup completions
    rustup completions fish >$XDG_CONFIG_HOME/fish/completions/rustup.fish

    echo installing chezmoi completions
    chezmoi completion fish --output=$XDG_CONFIG_HOME/fish/completions/chezmoi.fish

    echo installing pdm completions
    pdm completion fish >$XDG_CONFIG_HOME/fish/completions/pdm.fish
end
function _install_tide
    set -l _tide_tmp_dir (command mktemp -d)
    curl https://codeload.github.com/ilancosman/tide/tar.gz/v6 | tar -xzC $_tide_tmp_dir
    command cp -R $_tide_tmp_dir/*/{completions,conf.d,functions} $__fish_config_dir
    fish_path=(status fish-path) exec $fish_path -C "emit _tide_init_install"
end
